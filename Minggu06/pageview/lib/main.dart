import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: const MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final List<String> gambar = [
    "gambar01.jpg",
    "gambar02.jpg",
    "gambar03.jpg",
    "gambar04.jpg",
    "gambar05.jpg",
    "gambar06.png",
    "gambar07.jpg",
    "gambar08.jpg",
  ];

  static const Map<String, Color> colors = {
    'gambar01': Color(0xFF2DB569),
    'gambar02': Color(0xFFF38688),
    'gambar03': Color(0xFF45CAF5),
    'gambar04': Color(0xFFB19ECB),
    'gambar05': Color(0xFFF58E4C),
    'gambar06': Color(0xFF46C1BE),
    'gambar07': Color(0xFFFFEA0E),
    'gambar08': Color(0xFFDBE4E9),
  };

  @override
  Widget build(BuildContext context) {
    timeDilation = 5.0;
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: FractionalOffset.topCenter,
              end: FractionalOffset.bottomCenter,
              colors: [Colors.white, Colors.purple, Colors.deepPurple]),
        ),
        child: PageView.builder(
            controller: PageController(viewportFraction: 0.8),
            itemCount: gambar.length,
            itemBuilder: (BuildContext context, int i) {
              return Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 5.0, vertical: 50.0),
                  child: Material(
                    elevation: 8.0,
                    child: Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        Hero(
                            tag: gambar[i],
                            child: Material(
                              child: InkWell(
                                child: Flexible(
                                  flex: 1,
                                  child: Container(
                                    color: colors.values.elementAt(i),
                                    child: Image.asset(
                                      "assets/img/${gambar[i]}",
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                onTap: () => Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            Halamandua(
                                              gambar: gambar[i],
                                              colors:
                                                  colors.values.elementAt(i),
                                            ))),
                              ),
                            ))
                      ],
                    ),
                  ));
            }),
      ),
    );
  }
}

class Halamandua extends StatefulWidget {
  Halamandua({Key? key, required this.gambar, required this.colors})
      : super(key: key);
  final String gambar;
  final Color colors;

  @override
  State<Halamandua> createState() => _HalamanduaState();
}

class _HalamanduaState extends State<Halamandua> {
  Color warna = Colors.grey;

  void _pilihannya(Pilihan pilihan) {
    setState(() {
      warna = pilihan.warna;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("E41201740 - Afris Nurfal Aziz"),
        backgroundColor: Colors.purpleAccent,
        actions: <Widget>[
          PopupMenuButton<Pilihan>(
            onSelected: _pilihannya,
            itemBuilder: (BuildContext context) {
              return listPilihan.map((Pilihan x) {
                return PopupMenuItem<Pilihan>(
                  child: Text(x.teks),
                  value: x,
                );
              }).toList();
            },
          )
        ],
      ),
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                gradient: RadialGradient(
                    center: Alignment.center,
                    colors: [Colors.purple, Colors.white, Colors.deepPurple])),
          ),
          Center(
            child: Hero(
                tag: widget.gambar,
                child: ClipOval(
                    child: SizedBox(
                        width: 200.0,
                        height: 200.0,
                        child: Material(
                          child: InkWell(
                            onTap: () => Navigator.of(context).pop(),
                            child: Flexible(
                              flex: 1,
                              child: Container(
                                color: widget.colors,
                                child: Image.asset(
                                  "assets/img/${widget.gambar}",
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        )))),
          )
        ],
      ),
    );
  }
}

class Pilihan {
  const Pilihan({required this.teks, required this.warna});
  final String teks;
  final Color warna;
}

List<Pilihan> listPilihan = const <Pilihan>[
  const Pilihan(teks: "Red", warna: Colors.red),
  const Pilihan(teks: "Purple", warna: Colors.purple),
  const Pilihan(teks: "Blue", warna: Colors.blue),
];
