import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(children: <Widget>[
        Flexible(
          flex: 1,
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.bottomLeft,
                end: Alignment.topRight,
                colors: <Color>[
                  Colors.yellow,
                  Colors.blue,
                ],
              ),
            ),
          ),
        ),
        Flexible(
          flex: 2,
          child: Container(
            decoration: BoxDecoration(
              gradient: RadialGradient(
                radius: 1.0,
                colors: <Color>[
                  Colors.yellow,
                  Colors.blue,
                  Colors.orange,
                ],
              ),
            ),
          ),
        ),
        Flexible(
          flex: 1,
          child: Container(
            decoration: BoxDecoration(
              gradient: SweepGradient(
                startAngle: 1.0,
                colors: <Color>[
                  Colors.yellow,
                  Colors.blue,
                  Colors.orange,
                ],
              ),
            ),
          ),
        ),
      ]),
    );
  }
}
