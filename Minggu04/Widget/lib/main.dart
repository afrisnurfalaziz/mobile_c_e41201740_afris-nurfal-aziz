import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Afris Nurfal Aziz'),
        ),
        body: Column(
          children: [
            text(),
            wow(),
            btn(),
            textfield(),
            img(),
          ],
        ),
      ),
    );
  }
}

Widget text() {
  return const Center(
    child: Text(
      "Ini Text Widget",
      style: TextStyle(
        color: Colors.red,
        backgroundColor: Colors.black,
        fontSize: 30.0,
        fontStyle: FontStyle.italic,
        fontWeight: FontWeight.bold,
      ),
    ),
  );
}

Widget wow() {
  return Container(
    margin: EdgeInsets.all(16.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Column(
          children: <Widget>[Icon(Icons.access_alarm), Text('Alarm')],
        ),
        Column(
          children: <Widget>[Icon(Icons.phone), Text('Phone')],
        ),
        Column(
          children: <Widget>[Icon(Icons.book), Text('Book')],
        ),
      ],
    ),
  );
}

Widget btn() {
  return Center(
    child: Column(
      children: <Widget>[
        RaisedButton(
          color: Colors.amber,
          child: Text("Raised Button"),
          onPressed: () {},
        ),
        MaterialButton(
          color: Colors.lime,
          child: Text("Material Button"),
          onPressed: () {},
        ),
        FlatButton(
          color: Colors.lightGreenAccent,
          child: Text("FlatButton Button"),
          onPressed: () {},
        ),
      ],
    ),
  );
}

Widget textfield() {
  return Container(
    padding: const EdgeInsets.all(8.0),
    child: Form(
      child: Column(
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(hintText: "Username"),
          ),
          TextFormField(
            obscureText: true,
            decoration: InputDecoration(hintText: "Password"),
          ),
          const SizedBox(height: 30),
          RaisedButton(
            child: Text("Login"),
            onPressed: () {},
          )
        ],
      ),
    ),
  );
}

Widget img() {
  return Expanded(
    child: Center(
      child: Image.asset('assets/img/kelinci.jpg'),
    ),
  );
}
