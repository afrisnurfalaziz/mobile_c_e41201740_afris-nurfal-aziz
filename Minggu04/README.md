E41201740_Afris Nurfal Aziz

WIREFRAME APP
![wireframe app](https://user-images.githubusercontent.com/74905155/158808664-acec0571-df93-4d6e-aa25-3965d5d26d45.jpeg)

DESAIN MOCK UP APP
![desain app figma](https://user-images.githubusercontent.com/74905155/158808670-b66bfd0e-38f7-4f48-bf17-540968bfda67.jpeg)

WIDGET CORE COMPONENT
![widget](https://user-images.githubusercontent.com/74905155/158808674-e3bc230e-b9ba-48a5-95d5-8507a11bc274.jpeg)

UI TELEGRAM 01
![telegram2](https://user-images.githubusercontent.com/74905155/158808677-83f174cc-0de4-4222-ae94-1311f2766ad3.jpeg)

UI TELEGRAM 02
![telegram1](https://user-images.githubusercontent.com/74905155/158808679-dbdcafa9-abe9-4dc0-9765-968ab479bf78.jpeg)
