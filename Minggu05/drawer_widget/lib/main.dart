import 'package:drawer_widget/profile.dart';
import 'package:flutter/material.dart';
import 'package:drawer_widget/bottom.dart';
import 'package:drawer_widget/profile.dart';
import 'package:drawer_widget/home.dart';
import 'package:drawer_widget/settings.dart';
import 'package:drawer_widget/exit.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BottomNav(),
      routes: {
        "/home": (BuildContext context) => const home(),
        "/profile": (BuildContext context) => const profile(),
        "/settings": (BuildContext context) => const settings(),
        "/exit": (BuildContext context) => const exit(),
      },
    );
  }
}
