import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({Key? key}) : super(key: key);

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(children: <Widget>[
        const UserAccountsDrawerHeader(
          accountName: Text("Afris Nurfal Aziz"),
          currentAccountPicture: CircleAvatar(
            backgroundImage: AssetImage('assets/img/iqbal.jpeg'),
          ),
          accountEmail: Text("afrisazizi@gmail.com"),
        ),
        DrawerListTile(
          iconData: Icons.home,
          title: "Home",
          onTilePressed: () {
            Navigator.pushNamed(context, "/home");
          },
        ),
        DrawerListTile(
          iconData: Icons.account_circle,
          title: "Profile",
          onTilePressed: () {
            Navigator.pushNamed(context, "/profile");
          },
        ),
        DrawerListTile(
          iconData: Icons.settings,
          title: "Settings",
          onTilePressed: () {
            Navigator.pushNamed(context, "/settings");
          },
        ),
        DrawerListTile(
          iconData: Icons.exit_to_app,
          title: "Logout",
          onTilePressed: () {
            Navigator.pushNamed(context, "/exit");
          },
        ),
      ]),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;

  const DrawerListTile(
      {Key? key,
      required this.iconData,
      required this.title,
      required this.onTilePressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: const TextStyle(fontSize: 16),
      ),
    );
  }
}
