import 'package:flutter/material.dart';
import 'package:button_tab_nav/routes.dart';

void main() {
  runApp(MaterialApp(
    onGenerateRoute: RouteGenerator.generateRoute,
  ));
}
