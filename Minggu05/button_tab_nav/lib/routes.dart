import 'package:flutter/material.dart';
import 'package:button_tab_nav/screen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
// jika ingin mengirim argument
// final args = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => HomePage());
      case '/about':
        return MaterialPageRoute(builder: (_) => AboutPage());
// return MaterialPageRoute(builder: (_) => AboutPage(args));
      default:
        return _halamanLain();
    }
  }

  static Route<dynamic> _halamanLain() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(title: Text("Help")),
        body: Center(child: Text('INI HALAMAN HELP')),
      );
    });
  }
}
