E41201740 - Afris Nurfal Aziz

Output Acara 01 Flutter Styling
![flutter style output](https://user-images.githubusercontent.com/74905155/160240006-dcf102b0-2929-43ae-87e7-42f6b92d351f.jpg)

Output Acara 02 Flutter Navigation
![flutter navigator output 01](https://user-images.githubusercontent.com/74905155/160240058-49e24a6a-c36f-43f9-928c-5b6034f6e22c.jpg)
![flutter navigator output 02](https://user-images.githubusercontent.com/74905155/160240060-1f444531-f251-4bda-94a8-18394f851768.jpg)

Output Acara 03 Button Tab Navigation
![btv output 01](https://user-images.githubusercontent.com/74905155/160240088-91515eaa-c736-430d-abe3-668055fd7875.jpg)
![btv output 02](https://user-images.githubusercontent.com/74905155/160240091-4b59ca62-df79-47da-8523-23bee57edfd2.jpg)
![btv output 03](https://user-images.githubusercontent.com/74905155/160240093-831d6cbf-4586-4d31-929a-e6d29ffce81d.jpg)

Output Acara 04 Drawer Widget
![botnav home](https://user-images.githubusercontent.com/74905155/160240130-e716aaba-1015-4d10-b3ca-23e1690b44e0.jpg)
![botnav profile ](https://user-images.githubusercontent.com/74905155/160240133-c0885fe0-a9be-4927-bb26-751aff131d12.jpg)
![botnav settings](https://user-images.githubusercontent.com/74905155/160240136-d156a7f7-0cca-49ef-995c-eedfde357f7f.jpg)
![botnav logout](https://user-images.githubusercontent.com/74905155/160240132-55a59409-8ea0-4153-8d64-adf14fa597da.jpg)
![drawer](https://user-images.githubusercontent.com/74905155/160240146-14aeb5f9-7da2-4668-9729-5f6d9d2eb642.jpg)
![drawer home](https://user-images.githubusercontent.com/74905155/160240140-18ef3a08-aa50-4ae8-aa65-f1b5d0e36321.jpg)
![drawer profile](https://user-images.githubusercontent.com/74905155/160240143-bd00a7b8-c67e-4607-8ef2-60c0453d74c8.jpg)
![drawer settings](https://user-images.githubusercontent.com/74905155/160240144-34a0c3c0-5cc6-491b-9594-ea0b0d9a4542.jpg)
![drawer exit](https://user-images.githubusercontent.com/74905155/160240139-5d05e9c7-5b86-44c6-8c59-15fef403cd0e.jpg)
