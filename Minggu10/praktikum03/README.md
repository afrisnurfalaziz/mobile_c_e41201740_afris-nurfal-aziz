# praktikum03

Pengaturan Android Manifest, Asset dan Activity


![praktikum 03](https://user-images.githubusercontent.com/74905155/167044005-38229fb1-61b4-43c1-9e5d-8471ea7bd890.jpg)
![praktikum 03_2](https://user-images.githubusercontent.com/74905155/167044001-d6fa5441-9d90-470c-bf62-c85013bf1dfa.jpg)


## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
