import 'package:flutter/material.dart';
import 'package:form/praktikum01_02.dart';
import 'package:form/praktikum03.dart';
import 'package:form/praktikum04.dart';
import 'package:form/bottom.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BottomNav(),
      routes: {
        "/praktikum01_02": (BuildContext context) => const praktikumSatuDua(),
        // "/acara02": (BuildContext context) => const praktikumDua(),
        "/praktikum03": (BuildContext context) => const praktikumTiga(),
        "/praktikum04": (BuildContext context) => const praktikumEmpat(),
      },
    );
  }
}
