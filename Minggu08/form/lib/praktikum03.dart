import 'package:flutter/material.dart';

class praktikumTiga extends StatefulWidget {
  const praktikumTiga({Key? key}) : super(key: key);

  @override
  State<praktikumTiga> createState() => _praktikumTiga();
}

class _praktikumTiga extends State<praktikumTiga> {
  List<String> agama = [
    "Islam",
    "Kristen",
    "Hindu",
    "Budha",
    "konghucu",
  ];

  String _agama = "Islam";
  String _jk = "";

  TextEditingController controllerNama = TextEditingController();
  TextEditingController controllerPass = TextEditingController();
  TextEditingController controllerMoto = TextEditingController();

  void _pilihJk(String value) {
    setState(() {
      _jk = value;
    });
  }

  void _pilihAgama(String value) {
    setState(() {
      _agama = value;
    });
  }

  void kirimData() {
    AlertDialog alertDialog = new AlertDialog(
      content: Container(
        height: 200.0,
        child: Column(
          children: [
            Text("Nama Lengkap : ${controllerNama.text}"),
            Text("Password : ${controllerPass.text}"),
            Text("Moto Hidup : ${controllerMoto.text}"),
            Text("Jenis Kelamin : ${_jk}"),
            Text("Agama : ${_agama}"),
            RaisedButton(
              child: Text("OK"),
              onPressed: () => Navigator.pop(context),
              color: Colors.teal,
            )
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      padding: EdgeInsets.all(20.0),
      child: ListView(
        children: [
          TextField(
            controller: controllerNama,
            decoration: InputDecoration(
                hintText: "Nama Lengkap",
                labelText: "Nama Lengkap",
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20.0))),
          ),
          SizedBox(
            height: 20.0,
          ),
          TextField(
            controller: controllerPass,
            obscureText: true,
            decoration: InputDecoration(
                hintText: "Password",
                labelText: "Password",
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20.0))),
          ),
          SizedBox(
            height: 20.0,
          ),
          TextField(
            controller: controllerMoto,
            maxLines: 3,
            decoration: InputDecoration(
                hintText: "Moto Hidup",
                labelText: "Moto Hidup",
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20.0))),
          ),
          SizedBox(
            height: 20.0,
          ),
          RadioListTile(
            value: "laki-laki",
            title: Text("Laki-Laki"),
            groupValue: _jk,
            onChanged: (String? value) {
              _pilihJk(value!);
            },
            activeColor: Colors.blue,
            subtitle: Text("Pilih ini jika Anda laki-laki"),
          ),
          RadioListTile(
            value: "perepmpuan",
            title: Text("Perempuan"),
            groupValue: _jk,
            onChanged: (String? value) {
              _pilihJk(value!);
            },
            activeColor: Colors.blue,
            subtitle: Text("Pilih ini jika Anda perempuan"),
          ),
          SizedBox(
            height: 20.0,
          ),
          Row(
            children: [
              Text(
                "Agama",
                style: TextStyle(fontSize: 18.0, color: Colors.blue),
              ),
              SizedBox(
                width: 20.0,
              ),
              DropdownButton(
                onChanged: (String? value) {
                  _pilihAgama(value!);
                },
                value: _agama,
                items: agama.map((String value) {
                  return DropdownMenuItem(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              )
            ],
          ),
          Container(
            width: 350,
            height: 40,
            margin: EdgeInsets.all(20.0),
            child: TextButton(
              style: TextButton.styleFrom(
                backgroundColor: Colors.blue,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25.0)),
              ),
              child: Text(
                "OK",
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                kirimData();
              },
            ),
          ),
        ],
      ),
    ));
  }
}
