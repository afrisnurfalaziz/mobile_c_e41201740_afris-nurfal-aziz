import 'package:flutter/material.dart';

class praktikumSatuDua extends StatefulWidget {
  const praktikumSatuDua({Key? key}) : super(key: key);

  @override
  State<praktikumSatuDua> createState() => _praktikumSatuDua();
}

class _praktikumSatuDua extends State<praktikumSatuDua> {
  final formKey = GlobalKey<FormState>();
  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: formKey,
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.all(10.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: "ex: Afris Nurfal Aziz",
                      labelText: "Nama Lengkap",
                      focusColor: Colors.blueGrey[900],
                      icon: Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Nama Tidak Boleh Kosong!';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10.0),
                  child: TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      hintText: "ex: alex@gmail.com",
                      labelText: "Email",
                      icon: Icon(Icons.email),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Email Tidak Boleh Kosong!';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10.0),
                  child: TextFormField(
                    keyboardType: TextInputType.phone,
                    decoration: InputDecoration(
                      labelText: "Nomor Telepon",
                      icon: Icon(Icons.phone),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Nomor Telepon Tidak Boleh Kosong!';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10.0),
                  child: TextFormField(
                    keyboardType: TextInputType.datetime,
                    decoration: InputDecoration(
                      labelText: "Tanggal Lahir",
                      icon: Icon(Icons.date_range),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Tanggal Lahir Tidak Boleh Kosong!';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10.0),
                  child: TextFormField(
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: "Password",
                      icon: Icon(Icons.security),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Password Tidak Boleh Kosong!';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20.0),
                  child: CheckboxListTile(
                    title: Text('Syarat & Ketentuan'),
                    subtitle: Text(
                        'Apakah Anda Setuju Dengan Persyaratan & Ketentuan yang Berlaku?'),
                    value: nilaiCheckBox,
                    activeColor: Colors.blue,
                    onChanged: (value) {
                      setState(() {
                        nilaiCheckBox = value!;
                      });
                    },
                  ),
                ),
                SwitchListTile(
                  title: Text('Tema'),
                  subtitle: Text('Dark'),
                  value: nilaiSwitch,
                  activeTrackColor: Colors.blueAccent[100],
                  activeColor: Colors.blue,
                  onChanged: (value) {
                    setState(() {
                      nilaiSwitch = value;
                    });
                  },
                ),
                Slider(
                  activeColor: Colors.blue,
                  value: nilaiSlider,
                  min: 0,
                  max: 100,
                  onChanged: (value) {
                    setState(() {
                      nilaiSlider = value;
                    });
                  },
                ),
                Container(
                  width: 350,
                  height: 40,
                  margin: EdgeInsets.all(20.0),
                  child: TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: Colors.blue,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25.0)),
                    ),
                    child: Text(
                      "SUBMIT",
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      if (formKey.currentState!.validate()) {}
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
