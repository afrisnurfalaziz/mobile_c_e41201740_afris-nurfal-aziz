import 'package:flutter/material.dart';

class praktikumEmpat extends StatefulWidget {
  const praktikumEmpat({Key? key}) : super(key: key);

  @override
  State<praktikumEmpat> createState() => _praktikumEmpat();
}

class _praktikumEmpat extends State<praktikumEmpat> {
  List<String> agama = [
    "Islam",
    "Kristen",
    "Hindu",
    "Budha",
    "konghucu",
  ];

  String _agama = "Islam";
  String _genre = "";

  TextEditingController controllerNama = TextEditingController();
  TextEditingController controllerTmptLahir = TextEditingController();
  TextEditingController controllerTglLahir = TextEditingController();
  TextEditingController controllerAlamat = TextEditingController();
  TextEditingController controllerEmail = TextEditingController();
  TextEditingController controllerNoTelepon = TextEditingController();
  TextEditingController controllerPass = TextEditingController();

  void _pilihGenre(String value) {
    setState(() {
      _genre = value;
    });
  }

  void _pilihAgama(String value) {
    setState(() {
      _agama = value;
    });
  }

  void kirimData() {
    AlertDialog alertDialog = AlertDialog(
      content: SizedBox(
        width: 300.0,
        height: 250.0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Nama Lengkap  :  ${controllerNama.text}"),
            Text("Tempat Lahir  :  ${controllerTmptLahir.text}"),
            Text("Tanggal Lahir  :  ${controllerTglLahir.text}"),
            Text("Jenis Kelamin  :  $_genre"),
            Text("Agama  :  $_agama"),
            Text("Alamat  :  ${controllerAlamat.text}"),
            Text("Email  :  ${controllerEmail.text}"),
            Text("Nomor Telepon  :  ${controllerNoTelepon.text}"),
            Text("Password  :  ${controllerPass.text}"),
            Center(
              child: Container(
                width: 60,
                height: 40,
                margin: const EdgeInsets.only(top: 30.0),
                child: TextButton(
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.blue,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0)),
                  ),
                  child: const Text(
                    "OK",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () => Navigator.pop(context),
                ),
              ),
            )
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }

  final formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: formKey,
        child: Container(
          padding: const EdgeInsets.all(20.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.all(10.0),
                  child: TextFormField(
                    controller: controllerNama,
                    decoration: InputDecoration(
                      labelText: "Nama Lengkap",
                      icon: const Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Nama Tidak Boleh Kosong!';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(10.0),
                  child: TextFormField(
                    controller: controllerTmptLahir,
                    decoration: InputDecoration(
                      labelText: "Tempat Lahir",
                      icon: const Icon(Icons.child_friendly),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Tempat Lahir Tidak Boleh Kosong!';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(10.0),
                  child: TextFormField(
                    controller: controllerTglLahir,
                    keyboardType: TextInputType.datetime,
                    decoration: InputDecoration(
                      labelText: "Tanggal Lahir",
                      icon: const Icon(Icons.date_range),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Tanggal Lahir Tidak Boleh Kosong!';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(10.0),
                  child: const Text(
                    "Jenis Kelamin",
                    style: TextStyle(
                      fontSize: 17,
                    ),
                  ),
                ),
                RadioListTile(
                  value: "Laki-Laki",
                  title: const Text("Laki-Laki"),
                  groupValue: _genre,
                  dense: true,
                  onChanged: (String? value) {
                    _pilihGenre(value!);
                  },
                  activeColor: Colors.blue,
                ),
                RadioListTile(
                  value: "Perepmpuan",
                  title: const Text("Perempuan"),
                  groupValue: _genre,
                  onChanged: (String? value) {
                    _pilihGenre(value!);
                  },
                  activeColor: Colors.blue,
                ),
                Container(
                  margin: const EdgeInsets.all(10.0),
                  child: Row(
                    children: [
                      const Text(
                        "Agama",
                        style: TextStyle(fontSize: 17.0),
                      ),
                      const SizedBox(
                        width: 20.0,
                      ),
                      DropdownButton(
                        onChanged: (String? value) {
                          _pilihAgama(value!);
                        },
                        value: _agama,
                        items: agama.map((String value) {
                          return DropdownMenuItem(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(10.0),
                  child: TextFormField(
                    controller: controllerAlamat,
                    maxLines: 3,
                    decoration: InputDecoration(
                      labelText: "Alamat",
                      icon: const Icon(Icons.location_on),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Alamat Tidak Boleh Kosong!';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(10.0),
                  child: TextFormField(
                    controller: controllerEmail,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      labelText: "Email",
                      icon: const Icon(Icons.email),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Email Tidak Boleh Kosong!';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(10.0),
                  child: TextFormField(
                    controller: controllerNoTelepon,
                    keyboardType: TextInputType.phone,
                    decoration: InputDecoration(
                      labelText: "Nomor Telepon",
                      icon: const Icon(Icons.phone),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Nomor Telepon Tidak Boleh Kosong!';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(10.0),
                  child: TextFormField(
                    controller: controllerPass,
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: "Password",
                      icon: const Icon(Icons.security),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Password Tidak Boleh Kosong!';
                      }
                      return null;
                    },
                  ),
                ),
                const SizedBox(
                  height: 20.0,
                ),
                Center(
                  child: Container(
                    width: 350,
                    height: 40,
                    margin: const EdgeInsets.all(20.0),
                    child: TextButton(
                      style: TextButton.styleFrom(
                        backgroundColor: Colors.blue,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25.0)),
                      ),
                      child: const Text(
                        "SUBMIT",
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        if (formKey.currentState!.validate()) {
                          kirimData();
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
