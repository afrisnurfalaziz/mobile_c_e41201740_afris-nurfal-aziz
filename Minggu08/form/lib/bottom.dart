import 'package:flutter/material.dart';
import 'package:form/praktikum01_02.dart';
import 'package:form/praktikum03.dart';
import 'package:form/praktikum04.dart';

class BottomNav extends StatefulWidget {
  const BottomNav({Key? key}) : super(key: key);

  @override
  State<BottomNav> createState() => _BottomNav();
}

class _BottomNav extends State<BottomNav> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    praktikumSatuDua(),
    // praktikumDua(),
    praktikumTiga(),
    praktikumEmpat(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('E41201740 - Afris Nurfal Aziz'),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.blue,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.ac_unit_rounded),
            label: 'Praktikum 01 & 02',
          ),
          // BottomNavigationBarItem(
          //   icon: Icon(Icons.agriculture_rounded),
          //   label: 'Acara 02',
          // ),
          BottomNavigationBarItem(
            icon: Icon(Icons.looks_3),
            label: 'Praktikum 03',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.looks_4),
            label: 'Praktikum 04',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.white,
        onTap: _onItemTapped,
      ),
    );
  }
}
