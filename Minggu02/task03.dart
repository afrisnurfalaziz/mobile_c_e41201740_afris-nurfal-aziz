import 'dart:io';

void main() {
  stdout.write("First Name : ");
  var firstName = stdin.readLineSync();

  stdout.write("Middle Name : ");
  var middleName = stdin.readLineSync();

  stdout.write("Last Name : ");
  var lastName = stdin.readLineSync();

  print("Hello $firstName $middleName $lastName");
}
