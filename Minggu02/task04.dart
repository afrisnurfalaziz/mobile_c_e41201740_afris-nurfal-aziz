void main() {
  int a = 5;
  int b = 10;

  int hasilKali = a * b;
  double hasilBagi = a / b;
  int hasilTambah = a + b;
  int hasilKurang = a - b;

  print('Hasil Perkalian: $hasilKali');
  print('Hasil Pembagian: $hasilBagi');
  print('Hasil Penambahan: $hasilTambah');
  print('Hasil Pengurangan: $hasilKurang');
}
