import 'dart:io';

void main() {
  stdout.write("Masukkan Nama: ");
  var name = stdin.readLineSync();

  stdout.write("Masukkan Peran: ");
  var peran = stdin.readLineSync();

  if (name == '') {
    print('Nama Harus Diisi!');
  } else if (peran == '') {
    print('Halo $name, Pilih peranmu untuk memulai game!');
  } else if (peran == 'Penyihir') {
    print(
        'Selamat datang di Dunia Werewolf, $name\nHalo $peran $name, kamu dapat melihat siapa yang menjadi werewolf!');
  } else if (peran == 'Guard') {
    print(
        'Selamat datang di Dunia Werewolf, $name\nHalo $peran $name, kamu akan membantu melindungi temanmu dari serangan werewolf!');
  } else if (peran == 'Werewolf') {
    print(
        'Selamat datang di Dunia Werewolf, $name\nHalo $peran $name, Kamu akan memakan mangsa setiap malam!');
  } else {
    print('error!');
  }
}
